export default {
	getTicketInfo: (id, accessToken, successCallback, failureCallback) => {
		fetch('http://miraqueue.ko.tl/api/ticket/' + id + '?access_token=' + accessToken, {
			    headers: {  
			      "Content-type": "application/x-www-form-urlencoded; charset=UTF-8"  
                },
			})
			.then(
				function (response) {
					if (response.status !== 200) {
						failureCallback('Помилка сервера')
						return
					}
					response.json().then((data) => {
						if (typeof(data) !== 'undefined')
							successCallback(data)
						else
							failureCallback('Невідома помилка')
					})
				}
			)
			.catch((error) => {
				console.log(error)
			})
	},

	enter: (id, accessToken, successCallback, failureCallback) => {
		fetch('http://miraqueue.ko.tl/api/ticket/' + id + '/enter?access_token=' + accessToken, {
			headers: {  
			  "Content-type": "application/x-www-form-urlencoded; charset=UTF-8"  
			},
		})
		.then(
			function (response) {
				if (response.status !== 200) {
					failureCallback('Помилка сервера')
					return
				}
				response.json().then((data) => {
					if (typeof(data.error) !== 'undefined') {
						failureCallback(data.error)
						return
					}
					if (typeof(data.status) !== 'undefined')
						successCallback(data.status)
					else
						failureCallback('Невідома помилка')
				})
			}
		)
		.catch((error) => {
			console.log(error)
		})
	},

	leave: (id, accessToken, successCallback, failureCallback) => {
		fetch('http://miraqueue.ko.tl/api/ticket/' + id + '/leave?access_token=' + accessToken, {
			headers: {  
			  "Content-type": "application/x-www-form-urlencoded; charset=UTF-8"  
			},
		})
		.then(
			function (response) {
				if (response.status !== 200) {
					failureCallback('Помилка сервера')
					return
				}
				response.json().then((data) => {
					if (typeof(data.error) !== 'undefined') {
						failureCallback(data.error)
						return
					}
					if (typeof(data.status) !== 'undefined')
						successCallback(data.status)
					else
						failureCallback('Невідома помилка')
				})
			}
		)
		.catch((error) => {
			console.log(error)
		})
	},

	confirm: (id, accessToken, successCallback, failureCallback) => {
		fetch('http://miraqueue.ko.tl/api/ticket/' + id + '/confirm?access_token=' + accessToken, {
			headers: {  
			  "Content-type": "application/x-www-form-urlencoded; charset=UTF-8"  
			},
		})
		.then(
			function (response) {
				console.log(response)
				if (response.status !== 200) {
					failureCallback('Помилка сервера')
					return
				}
				response.json().then((data) => {
					console.log(data)
					if (typeof(data.error) !== 'undefined') {
						failureCallback(data.error)
						return
					}
					if (typeof(data.status) !== 'undefined')
						successCallback(data.status)
					else
						failureCallback('Невідома помилка')
				})
			}
		)
		.catch((error) => {
			console.log(error)
		})
	}
}