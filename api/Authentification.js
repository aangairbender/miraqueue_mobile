export default {
	login: (username, password, successCallback, failureCallback) => {
		fetch('http://miraqueue.ko.tl/api/login', {
				method: 'POST',
			    headers: {  
			      "Content-type": "application/x-www-form-urlencoded; charset=UTF-8"  
			    },  
				body: 'username=' + username + '&password=' + password
			})
			.then(
				function (response) {
					if (response.status !== 200) {
						failureCallback('Помилка сервера')
						return
					}
					response.json().then((data) => {
						if (typeof(data.error) !== 'undefined') {
							failureCallback(data.error)
							return
						}
						if ((typeof(data.access_token) !== 'undefined')&&(typeof(data.user_id) !== 'undefined'))
							successCallback(data)
						else
							failureCallback('Невідома помилка')
					})
				}
			)
			.catch((error) => {
				console.log(error)
			})
	}
}