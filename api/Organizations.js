export default {
	getOrganizations: (accessToken, successCallback, failureCallback) => {
		fetch('http://miraqueue.ko.tl/api/organizations?access_token=' + accessToken, {
			    headers: {  
			      "Content-type": "application/x-www-form-urlencoded; charset=UTF-8"  
			    },  
			})
			.then(
				function (response) {
					if (response.status !== 200) {
						failureCallback('Помилка сервера')
						return
					}
					response.json().then((data) => {
						if (typeof(data.error) !== 'undefined') {
							failureCallback(data.error)
							return
						}
						if (typeof(data.organizations) !== 'undefined')
							successCallback(data.organizations)
						else
							failureCallback('Невідома помилка')
					})
				}
			)
			.catch((error) => {
				console.log(error)
			})
	}
}