export default {
	getMyTickets: (accessToken, successCallback, failureCallback) => {
		fetch('http://miraqueue.ko.tl/api/ticket/queues?access_token=' + accessToken, {
			    headers: {  
			      "Content-type": "application/x-www-form-urlencoded; charset=UTF-8"  
			    },  
			})
			.then(
				function (response) {

					console.log(response)
					if (response.status !== 200) {
						failureCallback('Помилка сервера')
						return
					}
					response.json().then((data) => {
						console.log(data)
						if (typeof(data.error) !== 'undefined') {
							failureCallback(data.error)
							return
						}
						if (typeof(data) !== 'undefined')
							successCallback(data)
						else
							failureCallback('Невідома помилка')
					})
				}
			)
			.catch((error) => {
				console.log(error)
			})
	}
}