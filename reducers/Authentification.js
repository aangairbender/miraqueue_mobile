
import {
	LOGIN_REQUEST,
	LOGIN_SUCCESS,
	LOGIN_FAILURE,
} from '../constants/ActionTypes'

const initialState = {
	loggedIn: false,
	loginInProcess: false,
	accessToken: '',
	lastError: '',
	username: ''
}

export const isLoggedInSelector = (state) => state.authentification.loggedIn
export const isLoginInProcessSelector = (state) => state.authentification.loginInProcess
export const getUsernameSelector = (state) => state.authentification.username
export const getLastErrorSelector = (state) => state.authentification.lastError
export const getAccessTokenSelector = (state) => state.authentification.accessToken

const authentificationReducer = (state = initialState, action) => {
	switch (action.type) {
		case LOGIN_REQUEST:
			return Object.assign({}, state, {
				loginInProcess: true,
				username: action.username
			})
		case LOGIN_SUCCESS:
			/*client.on('connect', function () {
				client.subscribe('user' + action.userID)
			})
			client.on('message', function(topic, message){
				console.log(JSON.parse(message))
			})*/
			return Object.assign({}, state, {
				loggedIn: true,
				loginInProcess: false,
				accessToken: action.accessToken,
				lastError: ""
			})
		case LOGIN_FAILURE:
			return Object.assign({}, state, {
				loggedIn: false,
				loginInProcess: false,
				lastError: action.error
			})
		default:
			return state
	}
}

export default authentificationReducer