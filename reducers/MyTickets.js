
import {
    MYTICKETS_REQUEST,
    MYTICKETS_SUCCESS,
    MYTICKETS_FAILURE
} from '../constants/ActionTypes'

const initialState = {
	loading: false,
	lastError: '',
    myTickets: []
}

export const isLoadingSelector = (state) => state.myTickets.loading
export const getMyTicketsSelector = (state) => state.myTickets.myTickets
export const getLastErrorSelector = (state) => state.myTickets.lastError

const myTicketsReducer = (state = initialState, action) => {
	switch (action.type) {
		case MYTICKETS_REQUEST:
			return Object.assign({}, state, {
				loading: true
			})
		case MYTICKETS_SUCCESS:
			return Object.assign({}, state, {
				loading: false,
				myTickets: action.myTickets
			})
		case MYTICKETS_FAILURE:
			return Object.assign({}, state, {
                loading: false,
				lastError: action.error
			})
		default:
			return state
	}
}

export default myTicketsReducer