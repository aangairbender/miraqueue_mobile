import { combineReducers } from 'redux'
import authentificationReducer from './Authentification'
import organizationsReducer from './Organizations'
import ticketInfoReducer from './TicketInfo'
import myTicketsReducer from './MyTickets'
import notificationsReducer from './Notifications'


const rootReducer = combineReducers({
	authentification: authentificationReducer,
	organizations: organizationsReducer,
    ticketInfo: ticketInfoReducer,
    myTickets: myTicketsReducer,
    notifications: notificationsReducer
})

export default rootReducer