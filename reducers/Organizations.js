
import {
	ORGANIZATIONS_REQUEST,
	ORGANIZATIONS_SUCCESS,
	ORGANIZATIONS_FAILURE
} from '../constants/ActionTypes'

const initialState = {
	loading: false,
	lastError: '',
    organizations: []
}

export const isLoadingSelector = (state) => state.organizations.loading
export const getOrganizationsSelector = (state) => state.organizations.organizations
export const getLastErrorSelector = (state) => state.organizations.lastError

const organizationsReducer = (state = initialState, action) => {
	switch (action.type) {
		case ORGANIZATIONS_REQUEST:
			return Object.assign({}, state, {
				loading: true
			})
		case ORGANIZATIONS_SUCCESS:
			return Object.assign({}, state, {
				loading: false,
				organizations: action.organizations
			})
		case ORGANIZATIONS_FAILURE:
			return Object.assign({}, state, {
				loading: false,
				lastError: action.error
			})
		default:
			return state
	}
}

export default organizationsReducer