
import {
    TICKETINFO_REQUEST,
    TICKETINFO_SUCCESS,
	TICKETINFO_FAILURE,    
	ON_ENTER_REQUEST,
	ON_ENTER_SUCCESS,
	ON_ENTER_FAILURE,
	ON_LEAVE_REQUEST,
	ON_LEAVE_SUCCESS,
	ON_LEAVE_FAILURE
} from '../constants/ActionTypes'

const initialState = {
	loading: false,
	enterLeaveLoading: false,
	lastError: '',
    ticketInfo: {}
}

export const isTicketLoadingSelector = (state) => state.ticketInfo.loading
export const getTicketInfoSelector = (state) => state.ticketInfo.ticketInfo
export const isEnterLeaveLoadingSelector = (state) => state.ticketInfo.enterLeaveLoading
export const getLastErrorSelector = (state) => state.ticketInfo.lastError

const ticketInfoReducer = (state = initialState, action) => {
	switch (action.type) {
		case TICKETINFO_REQUEST:
			return Object.assign({}, state, {
				loading: true
			})
		case TICKETINFO_SUCCESS:
			return Object.assign({}, state, {
				loading: false,
				ticketInfo: action.ticketInfo
			})
		case TICKETINFO_FAILURE:
			return Object.assign({}, state, {
				loading: false,
				lastError: action.error
			})
		case ON_ENTER_REQUEST:
		case ON_LEAVE_REQUEST:
			return Object.assign({}, state, {
				enterLeaveLoading: true
			})
		case ON_ENTER_SUCCESS:
		case ON_LEAVE_SUCCESS:
			return Object.assign({}, state, {
				enterLeaveLoading: false
			})
		case ON_ENTER_FAILURE:
		case ON_LEAVE_FAILURE:
			return Object.assign({}, state, {
				enterLeaveLoading: false,
				lastError: action.error
			})
		default:
			return state
	}
}

export default ticketInfoReducer