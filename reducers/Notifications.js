
import {
    NOTIFICATIONS_TOKEN_ADD
} from '../constants/ActionTypes'

const initialState = {
	token: ''
}

export const getNotificationsTokenSelector = (state) => state.notifications.token

const notificationsReducer = (state = initialState, action) => {
	switch (action.type) {
		case NOTIFICATIONS_TOKEN_ADD:
			return Object.assign({}, state, {
				token: action.token
			})
		default:
			return state
	}
}

export default notificationsReducer