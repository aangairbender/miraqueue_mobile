import React, { Component } from 'react'
import { Provider } from 'react-redux'
import { StyleSheet, Text, View, Button } from 'react-native'
import configureStore from './store/configureStore'
import LoginContainer from './containers/LoginContainer'
import OrganizationsContainer from './containers/OrganizationsContainer'
import TicketInfoContainer from './containers/TicketInfoContainer'
import OrganizationInfo from './components/OrganizationInfo'
import MyTicketsContainer from './containers/MyTicketsContainer'
import { processMessage } from './actions/Notifications'
import {
  createSwitchNavigator,
  createStackNavigator,
  createBottomTabNavigator,
  createDrawerNavigator,
  DrawerActions } from 'react-navigation'
import { Ionicons, FontAwesome } from '@expo/vector-icons'


const store = configureStore({})



const AppStack = createDrawerNavigator({
  organizations: {
    screen: OrganizationsContainer,
    navigationOptions: ({navigation}) => ({
      title: 'Організації'
    })
  },
  'mytickets': {
    screen: MyTicketsContainer,
    navigationOptions: ({navigation}) => ({
      title: 'Мої черги'
    })
  }})
const DrawerStack = createStackNavigator(
  {
    'main': AppStack,
    'organizationInfo' : OrganizationInfo,
    'ticketInfo': TicketInfoContainer
  },
  {
    headerMode: 'float',
    navigationOptions: ({navigation}) => ({
      headerStyle: {backgroundColor: '#3286aa'},
      title: 'Без черги',
      headerTintColor: 'white',
      headerLeft: <Ionicons style={{marginLeft:12}} name='ios-menu' color='white' size={32} onPress={()=>{navigation.dispatch(DrawerActions.toggleDrawer())}}/>
    })
  }
)

const Router = createSwitchNavigator(
  {
    'auth': LoginContainer,
    'drawer': DrawerStack
  },
  {
    initialRouteName: 'auth'
  }
)

class App extends Component {

  

  render() {
    return (
      <Provider store={store}>
          <Router/>
      </Provider>
    )
  }

  


}


export default App