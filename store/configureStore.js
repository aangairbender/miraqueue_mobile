import thunkMiddleware from 'redux-thunk'
import { createStore, applyMiddleware } from 'redux'
import rootReducer from '../reducers'


const configureStore = (initialState : Object) => {
	let store = createStore(rootReducer, initialState, applyMiddleware(thunkMiddleware));
	return store
}

export default configureStore