import React, { Component } from 'react'
import {
	Text,
	TextInput,
	View,
	Button,
	ScrollView,
	Modal,
	ActivityIndicator,
	StyleSheet,
	TouchableOpacity
} from 'react-native'
import PropTypes from 'prop-types'
import { withNavigation } from 'react-navigation'

class OrganizationCard extends Component {

	constructor(props) {
		super(props)
	}

	render() {

		let data = this.props.data

		return (
			<TouchableOpacity
				style={styles.card}
				onPress={() => {
					this.props.navigation.push('organizationInfo', {data: data})
				}}>
				<Text style={styles.cardLabel}>{data.name}</Text>
			</TouchableOpacity>
		)
	}
}

OrganizationCard.propTypes = {
	data: PropTypes.object,
	key: PropTypes.number
}

const styles = StyleSheet.create ({
	card: {
		flex: 1,
		justifyContent: 'center',
		borderWidth: 2,
		marginBottom: 2,
		height: 60,
		backgroundColor: '#dce9ef',
		borderColor: '#91bbd1',
		padding: 5,
	},
	cardLabel: {	
		color: '#00356a',
		fontSize: 24
	}
})

export default withNavigation(OrganizationCard)