import React, { Component } from 'react'
import {
	Text,
	TextInput,
	View,
	Button,
	ScrollView,
	Modal,
	ActivityIndicator,
	StyleSheet,
	TouchableOpacity
} from 'react-native'
import PropTypes from 'prop-types'
import { withNavigation } from 'react-navigation'
import TicketTreeViewer from './TicketTreeViewer'

class OrganizationInfo extends Component {

	constructor(props) {
		super(props)
	}

	render() {

		let data = this.props.navigation.getParam('data', {})

		return (
			<ScrollView style={styles.container}>
				<View style={styles.nameWrapper}>
					<Text style={styles.name}>{data.name}</Text>
				</View>
				<Text style={styles.description}>{data.description}</Text>
				<TicketTreeViewer tree={data.tickettree}/>
			</ScrollView>
		)
	}
}

OrganizationInfo.propTypes = {
	data: PropTypes.object
}

const styles = StyleSheet.create ({
	container: {
		flex: 1,
		padding: 5,
	},
	nameWrapper: {
		flex: 1,
		alignItems: 'center',
		marginBottom: 12
	},
	name: {
		fontSize: 24
	},
	description: {
		fontSize: 18,
	}
})

export default withNavigation(OrganizationInfo)