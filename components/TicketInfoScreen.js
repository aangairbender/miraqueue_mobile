import React, { Component } from 'react'
import {
	Text,
	TextInput,
	View,
	Button,
	ScrollView,
	Modal,
	ActivityIndicator,
	StyleSheet,
	TouchableOpacity
} from 'react-native'
import PropTypes from 'prop-types'
import { withNavigation } from 'react-navigation'

class TicketInfoScreen extends Component {

	constructor(props) {
		super(props)
		this.props.getTicketInfo(this.props.navigation.getParam('id', 0), this.props.accessToken)
		this.extraData = this.props.navigation.getParam('extraData', {})
		this.timer = setInterval(() => {
			this.props.getTicketInfo(this.props.navigation.getParam('id', 0), this.props.accessToken)
		}, 1000 * 10)
	}

	componentWillUnmount() {
		clearInterval(this.timer)
	}

	render() {
		if (this.props.ticketLoading) {//rgba was alpha 0.8
			return (
				<Modal
					visible={this.props.ticketLoading}
					transparent={true}
					animationType={'none'}
					onRequestClose={()=>{}}>
					<View style={{ flex:1, justifyContent: 'center', alignItems: 'center', 'backgroundColor': 'rgba(52,52,52,0.0)' }}>
						<ActivityIndicator
							animating={this.props.ticketLoading}
							size={'large'}/>
					</View>
				</Modal>
			)
		} else {
			let ticket = this.props.ticket

			let enterButton = () => {
				if (ticket.can_enter) {
					return (
						<View style={styles.buttonWrapper}>
							<Button
								style={styles.button}
								title='Стати в чергу'
								color={'#006600'}
								onPress={()=>{
									this.props.onEnter(ticket.id, this.props.accessToken)
								}}/>
						</View>
					)
				} else {
					return null
				}
			}


			let leaveButton = () => {
				if (ticket.can_leave) {
					return (
						<View style={styles.buttonWrapper}>
							<Button
								title='Вийти з черги'
								color={'#FF3333'}
								onPress={()=>{
									this.props.onLeave(ticket.id, this.props.accessToken)		
								}}/>
						</View>
					)
				} else {
					return null
				}
			}

			let confirmButton = () => {
				if (this.extraData.need_confirmation == '1') {
					return (
						<View style={styles.buttonWrapper}>
							<Button
								title='Підтвердити, що я прийду'
								color={'#006600'}
								onPress={()=>{
									this.props.onConfirm(ticket.id, this.props.accessToken)
									this.extraData.need_confirmation = '2'	
								}}/>
						</View>
					)
				} else {
					return null
				}
			}

			return (
				<ScrollView style={styles.container}>
					<View style={styles.nameWrapper}>
						<Text style={styles.name}>{ticket.name}</Text>
					</View>
					<Text style={styles.description}>{ticket.description}</Text>
					<Text style={styles.duration}>{'Середній час опрацювання одного клієнту: ' + ticket.base_duration}</Text>
					{confirmButton()}
					{enterButton()}
					{leaveButton()}
				</ScrollView>
			)
		}
	}
}

TicketInfoScreen.propTypes = {
	id: PropTypes.number, //dont need to pass from container
	getTicketInfo: PropTypes.func,
	ticketLoading: PropTypes.bool,
	ticket: PropTypes.object,
	accessToken: PropTypes.string,
	onEnter: PropTypes.func,
	onLeave: PropTypes.func,
	onConfirm: PropTypes.func,
	enterLeaveLoading: PropTypes.bool //whether is entering or leaving is loading
}

const styles = StyleSheet.create ({
	container: {
		flex: 1,
		padding: 5,
	},
	nameWrapper: {
		flex: 1,
		alignItems: 'center',
		marginBottom: 12
	},
	name: {
		fontSize: 24
	},
	description: {
		fontSize: 18,
	},
	duration: {
		fontSize: 18,
		marginTop: 5,
		marginBottom: 10,
	},
	buttonWrapper: {
		height: 60,
	}
})

export default withNavigation(TicketInfoScreen)