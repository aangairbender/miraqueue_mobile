import React, { Component } from 'react'
import {
	Text,
	TextInput,
	View,
	Button,
	ScrollView,
	Modal,
	ActivityIndicator,
	StyleSheet,
	TouchableOpacity
} from 'react-native'
import PropTypes from 'prop-types'
import { withNavigation } from 'react-navigation'

class TicketCard extends Component {

	constructor(props) {
		super(props)
		this.state = {
			estimation: props.data.estimation
		}
	}

	componentDidMount() {
		this.timer = setInterval(() => {
			this.setState(oldState => ({
				estimation: Math.max(0, oldState.estimation - 1)
			}))
		}, 1000 * 60)
	}

	componentWillUnmount() {
		clearInterval(this.timer)
	}



	render() {

		let data = this.props.data

		let timeViewer = (minutes) => {
			let hours = parseInt(minutes / 60)
			minutes = minutes % 60
			return <Text>{hours} год. {minutes} хв.</Text>
		}

		let posOrMustGo = () => {
			if (data.must_enter) {
				return (
					<View>
						<Text style={[styles.info, styles.importantInfo]}>Увага, Ваша черга</Text>
						<Text style={styles.info}>Підійдіть до вікна {data.window}</Text>
					</View>
				)
			} else {
				return (
					<View>
						<Text style={styles.info}>Позиція в черзі: {data.position} / {data.total}</Text>
						<Text style={styles.info}>Приблизний час очікування: {timeViewer(this.state.estimation)}</Text>
					</View>
				)
			}
		}

		return (
			<TouchableOpacity
				style={styles.card}
				onPress={() => {
					this.props.navigation.push('ticketInfo', {id: data.ticket_id, extraData: data})
				}}>
				<Text style={styles.cardLabel}>{data.name}</Text>
				{posOrMustGo()}
			</TouchableOpacity>
		)
	}
}

TicketCard.propTypes = {
	data: PropTypes.object,
	key: PropTypes.number
}

const styles = StyleSheet.create ({
	card: {
		flex: 1,
		justifyContent: 'center',
		borderWidth: 2,
		marginBottom: 2,
		height: 80,
		backgroundColor: '#dce9ef',
		borderColor: '#91bbd1',
		padding: 5,
	},
	cardLabel: {	
		color: '#00356a',
		fontSize: 24
	},
	info: {
		fontSize: 16,
		color: '#3286aa'
	},
	importantInfo: {
		color: '#ff3333'
	}
})

export default withNavigation(TicketCard)