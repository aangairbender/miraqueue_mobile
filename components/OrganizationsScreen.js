import React, { Component } from 'react'
import {
	Text,
	TextInput,
	View,
	Button,
	ScrollView,
	Modal,
	ActivityIndicator,
	StyleSheet	
} from 'react-native'
import PropTypes from 'prop-types'
import OrganizationCard from './OrganizationCard'


class OrganizationsScreen extends Component {


	constructor(props) {
		super(props)
		
		this.props.getOrganizations(this.props.accessToken)
	}



	render() {
		if (this.props.loading) {
			return (
				<Modal
					visible={this.props.loading}
					transparent={false}
					animationType={'none'}
					onRequestClose={()=>{}}>
					<View style={{ flex:1, justifyContent: 'center', alignItems: 'center' }}>
						<ActivityIndicator
							animating={this.props.loading}
							size={'large'}/>
					</View>
				</Modal>
			)
		} else {
			return (
				<View>
					<ScrollView>
						{this.props.organizations.map((item, i) => 
							<OrganizationCard data={item} key={i}/>
						)}
					</ScrollView>
				</View>
			)
		}
	}
}

OrganizationsScreen.propTypes = {
	getOrganizations: PropTypes.func,
	loading: PropTypes.bool,
	organizations: PropTypes.array,
	accessToken: PropTypes.string
}

export default OrganizationsScreen