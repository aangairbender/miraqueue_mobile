import React, { Component } from 'react'
import {
	Text,
	TextInput,
	View,
	Button,
	KeyboardAvoidingView,
	Modal,
	ActivityIndicator,
	StyleSheet,
	PixelRatio
} from 'react-native'
import PropTypes from 'prop-types'

import { Permissions, Notifications } from 'expo'

class LoginScreen extends Component {

	constructor(props) {
		super(props)
		this.state = {
			phone: '+380666666666',
			password: '12345'
		}
	}

	componentDidMount() {
		this.registerForPushNotifications()
	}
  
  	async registerForPushNotifications() {
	    const { status } = await Permissions.getAsync(Permissions.NOTIFICATIONS);

	    if (status !== 'granted') {
	      const { status } = await Permissions.askAsync(Permissions.NOTIFICATIONS);
	      if (status !== 'granted') {
	        return;
	      }
	    }

	    const token = await Notifications.getExpoPushTokenAsync();

	    this.subscription = Notifications.addListener(this.handleNotification);

	    this.props.addNotificationsToken(token)
	}

	handleNotification() {
		
	}

	componentDidUpdate() {
		if (this.props.loggedIn) {
			this.props.navigation.navigate('drawer')
		}
	}

	render() {
		return (
			<KeyboardAvoidingView style={styles.container}>
				<Modal
					visible={this.props.loginInProcess}
					transparent={true}
					animationType={'fade'}
					onRequestClose={()=>{}}>
					<View style={{ flex:1, justifyContent: 'center', alignItems: 'center', 'backgroundColor': 'rgba(52,52,52,0.8)' }}>
						<ActivityIndicator
							animating={this.props.loginInProcess}
							size={'large'}/>
					</View>
				</Modal>
				<View>
					<Text style={styles.label}>Номер телефону</Text>
					<TextInput
						style={styles.input}
						autoCorrect={false}
						underlineColorAndroid={'rgba(0,0,0,0)'}
						keyboardType={'phone-pad'}
						value={this.state.phone}
						onChangeText={(newValue) => {this.setState({phone: newValue})}}/>
				</View>
				<View>
					<Text style={styles.label}>Пароль</Text>
					<TextInput
						style={styles.input}
						autoCorrect={false}
						underlineColorAndroid={'rgba(0,0,0,0)'}
						secureTextEntry
						value={this.state.password}
						onChangeText={(newValue) => {this.setState({password: newValue})}}/>
				</View>
				<Text style={styles.errorLabel}>{this.props.lastError + ''}</Text>
				<View style={styles.btnWrapper}>
					<Button
						title="Вхід"
						style={styles.loginBtn}
						color={'#006600'}
						onPress={() => {this.props.onLogin(this.state.phone, this.state.password)}}/>
				</View>
			</KeyboardAvoidingView>
		)
	}
}

LoginScreen.propTypes = {
	onLogin: PropTypes.func,
	loginInProcess: PropTypes.bool,
	loggedIn: PropTypes.bool,
	lastError: PropTypes.string,
	addNotificationsToken: PropTypes.func
}

const styles = StyleSheet.create ({
	container: {
   		paddingTop: Expo.Constants.statusBarHeight,
   		backgroundColor: '#91bbd1',
   		flex: 1,
   		justifyContent: 'center',
   		alignItems: 'center'
    },
    input: {
    	height: 60,
    	width: PixelRatio.getPixelSizeForLayoutSize(130),
    	backgroundColor: '#dce9ef',
    	marginBottom: 10,
    	padding: 10,
    	color: '#3286aa',
    	fontSize: 24
	},
	label: {
		color: '#00356a',
		fontSize: 24,
		marginBottom: 5
	},
	loginBtn: {
	},
	btnWrapper: {	
		width: PixelRatio.getPixelSizeForLayoutSize(130),
		height: 60
	},
	errorLabel: {
		fontSize: 16,
		color: '#ff3333',
    	marginBottom: 10
	}
})

export default LoginScreen