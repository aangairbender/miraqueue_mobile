import React, { Component } from 'react'
import {
	Text,
	TextInput,
	View,
	Button,
	ScrollView,
	Modal,
	ActivityIndicator,
	StyleSheet,
} from 'react-native'
import PropTypes from 'prop-types'
import TicketCard from './TicketCard'

class MyTicketsScreen extends Component {

	constructor(props) {
		super(props)
		this.props.getMyTickets(this.props.accessToken)
		this.props.navigation.addListener(
			'didFocus',
			payLoad => {
				this.props.getMyTickets(this.props.accessToken)
			}
		)
	}

	componentDidUpdate() {
		if (!this.props.loading) {
			let tickets = this.props.myTickets
			tickets.forEach((t) => {
				if (t.need_confirmation == '1') {
					this.props.addNotification('Підтвердження', 'Треба підтвердити Вашу присутність у черзі за послугою ' + t.name, this.props.notificationsToken)
				}
			})
			
		}
	}

	render() {
		if (this.props.loading) {
			return (
				<Modal
					visible={this.props.loading}
					transparent={false}
					animationType={'none'}
					onRequestClose={()=>{}}>
					<View style={{ flex:1, justifyContent: 'center', alignItems: 'center' }}>
						<ActivityIndicator
							animating={this.props.loading}
							size={'large'}/>
					</View>
				</Modal>
			)
		} else {
			return (
				<View>
					<ScrollView>
						{this.props.myTickets.map((item, i) => 
							<TicketCard
								data={item}
								key={i}/>
						)}
					</ScrollView>
				</View>
			)
		}
	}
}

MyTicketsScreen.propTypes = {
	getMyTickets: PropTypes.func,
	loading: PropTypes.bool,
	myTickets: PropTypes.array,
	accessToken: PropTypes.string,
	addNotification: PropTypes.func,
	notificationsToken: PropTypes.string
}

export default MyTicketsScreen