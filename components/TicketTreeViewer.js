import React, { Component } from 'react'
import {
	Text,
	TextInput,
	View,
	Button,
	ScrollView,
	Modal,
	ActivityIndicator,
	StyleSheet,
	TouchableOpacity
} from 'react-native'
import PropTypes from 'prop-types'
import { withNavigation } from 'react-navigation'

class TicketTreeViewer extends Component {

	constructor(props) {
		super(props)
		this.state = {
			curNode: {
				name: '',
				children: JSON.parse(props.tree)
			},
			parents: []
		}
	}

	render() {

		let node = this.state.curNode
		let parents = this.state.parents
		let hasParent = (parents.length > 0)


		return (
			<View style={styles.container}>
				{ hasParent ? 
				<TouchableOpacity
					style={[styles.card, styles.backCard]}
					onPress={()=>{
						let parent = parents[parents.length - 1]
						parents.pop()
						this.setState({curNode: parent})
					}}>
					<Text style={styles.cardLabel}>Повернутися</Text>
				</TouchableOpacity>
				 : null}
				{node.children.map((child, i) => 
					<TouchableOpacity key={i}
						style={styles.card}
						onPress={() => {
							if (typeof(child.ticket_id) !== 'undefined') {

								this.props.navigation.push('ticketInfo', {id: child.ticket_id})
							} else {
								this.setState(oldState => ({
									parents: oldState.parents.concat(oldState.curNode),
									curNode: child
								}))
							}
						}}>
						<Text style={styles.cardLabel}>{child.name}</Text>
					</TouchableOpacity>
				)}
			</View>
		)
	}
}

TicketTreeViewer.propTypes = {
	tree: PropTypes.string
}


const styles = StyleSheet.create ({
	container: {
		flex: 1,
		marginTop: 24,
	},
	card: {
		flex: 1,
		justifyContent: 'center',
		borderWidth: 2,
		marginBottom: 2,
		height: 60,
		backgroundColor: '#dce9ef',
		borderColor: '#91bbd1',
		padding: 5,
	},
	backCard: {
		marginBottom: 8,
		height: 50,
	},
	cardLabel: {	
		color: '#00356a',
		fontSize: 24
	}
})

export default withNavigation(TicketTreeViewer)