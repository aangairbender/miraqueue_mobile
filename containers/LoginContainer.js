import { connect } from 'react-redux'
import LoginScreen  from '../components/LoginScreen'
import { login } from '../actions/Authentification'
import { addNotificationsToken } from '../actions/Notifications'
import { isLoggedInSelector, getLastErrorSelector, isLoginInProcessSelector } from '../reducers/Authentification'

const mapStateToProps = (state) => ({
	lastError: getLastErrorSelector(state),
	loginInProcess: isLoginInProcessSelector(state),
	loggedIn: isLoggedInSelector(state)
})

const mapDispatchToProps = (dispatch) => ({
	onLogin: (username, password) => dispatch(login(username, password)),
	addNotificationsToken: (token) => dispatch(addNotificationsToken(token))
})

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(LoginScreen)