import { connect } from 'react-redux'
import MyTicketsScreen  from '../components/MyTicketsScreen'
import { getMyTickets } from '../actions/MyTickets'
import { sendPushNotification } from '../actions/Notifications'
import { isLoadingSelector, getMyTicketsSelector, getLastErrorSelector } from '../reducers/MyTickets'
import { getAccessTokenSelector } from '../reducers/Authentification'
import { getNotificationsTokenSelector } from '../reducers/Notifications'

const mapStateToProps = (state) => ({
	loading: isLoadingSelector(state),
	myTickets: getMyTicketsSelector(state),
	accessToken: getAccessTokenSelector(state),
	lastError: getLastErrorSelector(state),
	notificationsToken: getNotificationsTokenSelector(state)
})

const mapDispatchToProps = (dispatch) => ({
	getMyTickets: (accessToken) => dispatch(getMyTickets(accessToken)),
	addNotification: (title, body, token) => dispatch(sendPushNotification(title, body, token))
})

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(MyTicketsScreen)