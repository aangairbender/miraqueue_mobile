import { connect } from 'react-redux'
import OrganizationsScreen  from '../components/OrganizationsScreen'
import { getOrganizations } from '../actions/Organizations'
import { isLoadingSelector, getOrganizationsSelector, getLastErrorSelector } from '../reducers/Organizations'
import { getAccessTokenSelector } from '../reducers/Authentification'

const mapStateToProps = (state) => ({
	loading: isLoadingSelector(state),
	organizations: getOrganizationsSelector(state),
	accessToken: getAccessTokenSelector(state),
	lastError: getLastErrorSelector(state)
})

const mapDispatchToProps = (dispatch) => ({
	getOrganizations: (accessToken) => dispatch(getOrganizations(accessToken))
})

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(OrganizationsScreen)