import { connect } from 'react-redux'
import TicketInfoScreen  from '../components/TicketInfoScreen'
import { getTicketInfo, onEnter, onLeave, onConfirm } from '../actions/TicketInfo'
import { getTicketInfoSelector, isTicketLoadingSelector, isEnterLeaveLoadingSelector, getLastErrorSelector } from '../reducers/TicketInfo'
import { getAccessTokenSelector } from '../reducers/Authentification'

const mapStateToProps = (state) => ({
	ticket: getTicketInfoSelector(state),
	ticketLoading: isTicketLoadingSelector(state),
	accessToken: getAccessTokenSelector(state),
	enterLeaveLoading: isEnterLeaveLoadingSelector(state),
	lastError: getLastErrorSelector(state)
})

const mapDispatchToProps = (dispatch) => ({
	getTicketInfo: (id, accessToken) => dispatch(getTicketInfo(id, accessToken)),
	onEnter: (id, accessToken) => dispatch(onEnter(id, accessToken)),
	onLeave: (id, accessToken) => dispatch(onLeave(id, accessToken)),
	onConfirm: (id, accessToken) => dispatch(onConfirm(id, accessToken)),
})

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(TicketInfoScreen)