import * as types from '../constants/ActionTypes'
import notifications from '../api/Notifications'

export const sendPushNotification = (title, body, token) => (dispatch) => {

		notifications.sendPushNotification(title, body, token)
}

export const addNotificationsToken = (token) => (dispatch) => {
	dispatch({
		type: types.NOTIFICATIONS_TOKEN_ADD,
		token
	})
}