import * as types from '../constants/ActionTypes'
import ticketInfo from '../api/TicketInfo'

export const getTicketInfo = (id, accessToken) => (dispatch) => {

	dispatch({
		type: types.TICKETINFO_REQUEST
    })


	ticketInfo.getTicketInfo(id, accessToken,
	(result) => {
		dispatch({
			type: types.TICKETINFO_SUCCESS,
			ticketInfo: result
		})
	},
	(result) => {
		dispatch({
			type: types.TICKETINFO_FAILURE,
			error: result
		})
	})
}

export const onEnter = (id, accessToken) => (dispatch) => {
	dispatch({
		type: types.ON_ENTER_REQUEST
	})

	ticketInfo.enter(id, accessToken,
	(result) => {
		dispatch({
			type: types.ON_ENTER_SUCCESS
		})
		dispatch(getTicketInfo(id, accessToken))
	},
	(result) =>{
		dispatch({
			type: types.ON_ENTER_FAILURE,
			error: result
		})
	})
}

export const onLeave = (id, accessToken) => (dispatch) => {
	dispatch({
		type: types.ON_LEAVE_REQUEST
	})

	ticketInfo.leave(id, accessToken,
	(result) => {
		dispatch({
			type: types.ON_LEAVE_SUCCESS
		})		
		dispatch(getTicketInfo(id, accessToken))
	},
	(result) =>{
		dispatch({
			type: types.ON_LEAVE_FAILURE,
			error: result
		})
	})
}


export const onConfirm = (id, accessToken) => (dispatch) => {

	ticketInfo.confirm(id, accessToken,
	(result) => {
		dispatch(getTicketInfo(id, accessToken))
	},

	(result) =>{
		
	})
}