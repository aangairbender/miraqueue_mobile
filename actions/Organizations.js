import * as types from '../constants/ActionTypes'
import organizations from '../api/Organizations'

export const getOrganizations = (accessToken) => (dispatch) => {

		dispatch({
				type: types.ORGANIZATIONS_REQUEST
		})

		organizations.getOrganizations(accessToken,
		(result) => {
				dispatch({
						type: types.ORGANIZATIONS_SUCCESS,
						organizations: result
				})
		},
		(result) => {
				dispatch({
						type: types.ORGANIZATIONS_FAILURE,
						error: result						
				})
		})
}