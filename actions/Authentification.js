import * as types from '../constants/ActionTypes'
import authentification from '../api/Authentification'

export const login = (username, password) => (dispatch) => {

	dispatch({
		type: types.LOGIN_REQUEST,
		username
	})

	authentification.login(username, password,
		(result) => {
			dispatch({
				type: types.LOGIN_SUCCESS,
				accessToken: result.access_token,
				userID: result.user_id
			})


		},
		(result) => {
			dispatch({
				type: types.LOGIN_FAILURE,
				error: result
			})
		}
	)
}

