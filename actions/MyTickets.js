import * as types from '../constants/ActionTypes'
import myTickets from '../api/MyTickets'

export const getMyTickets = (accessToken) => (dispatch) => {

		dispatch({
				type: types.MYTICKETS_REQUEST
		})

		myTickets.getMyTickets(accessToken,
		(result) => {
				dispatch({
						type: types.MYTICKETS_SUCCESS,
						myTickets: result
				})
		},
		(result) => {
				dispatch({
						type: types.MYTICKETS_FAILURE,
						error: result						
				})
		})
}